package everywhere.talk.checkout

interface HasItems {
    val items: List<CartItem>
}

data class HasItemsImpl(
    override val items: List<CartItem>
) : HasItems

sealed class CheckoutEvent {
    data class SelectItem(
        val item: CartItem
    ) : CheckoutEvent()

    object BeginCheckout : CheckoutEvent()

    data class SelectCard(
        val card: Card
    ) : CheckoutEvent()

    object VerifyCard : CheckoutEvent()

    data class ApplyCoupon(
        val couponCode: String
    ) : CheckoutEvent()

    object PlaceOrder : CheckoutEvent()

    object CancelCheckout : CheckoutEvent()
}

sealed class CheckoutMachine {
    object EmptyCart : CheckoutMachine()

    data class CartWithItems(
        override val items: List<CartItem>
    ) : CheckoutMachine(), HasItems by HasItemsImpl(items)

    data class NoCard(
        override val items: List<CartItem>
    ) : CheckoutMachine(), HasItems by HasItemsImpl(items)

    data class CardSelected(
        override val items: List<CartItem>,
        val card: Card
    ) : CheckoutMachine(), HasItems by HasItemsImpl(items)

    data class CardConfirmed(
        override val items: List<CartItem>,
        val card: Card
    ) : CheckoutMachine(), HasItems by HasItemsImpl(items)

    data class CouponApplied(
        override val items: List<CartItem>,
        val card: Card,
        val couponCode: String
    ) : CheckoutMachine(), HasItems by HasItemsImpl(items)

    data class OrderPlaced(
        val orderId: String
    ) : CheckoutMachine()
}

fun CheckoutMachine.handleEvent(event: CheckoutEvent): CheckoutMachine {
    return when (event) {
        is CheckoutEvent.SelectItem -> this.selectItem(event.item)
        CheckoutEvent.BeginCheckout -> this.beginCheckout()
        is CheckoutEvent.SelectCard -> this.selectCard(event.card)
        CheckoutEvent.VerifyCard -> this.verifyCard()
        CheckoutEvent.PlaceOrder -> this.placeOrder()
        CheckoutEvent.CancelCheckout -> this.cancelCheckout()
        is CheckoutEvent.ApplyCoupon -> TODO()
    }
}

private fun CheckoutMachine.selectItem(item: CartItem): CheckoutMachine {
    return when (this) {
        CheckoutMachine.EmptyCart ->
            CheckoutMachine.CartWithItems(listOf(item))
        is CheckoutMachine.CartWithItems ->
            CheckoutMachine.CartWithItems(this.items + item)
        is CheckoutMachine.CardSelected ->
            CheckoutMachine.CardSelected(this.items + item, this.card)

        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardConfirmed,
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.beginCheckout(): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.CartWithItems ->
            CheckoutMachine.NoCard(this.items)

        CheckoutMachine.EmptyCart,
        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardSelected,
        is CheckoutMachine.CardConfirmed,
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.selectCard(card: Card): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.NoCard ->
            CheckoutMachine.CardSelected(this.items, card)

        CheckoutMachine.EmptyCart,
        is CheckoutMachine.CartWithItems,
        is CheckoutMachine.CardSelected,
        is CheckoutMachine.CardConfirmed,
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.verifyCard(): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.CardSelected ->
            CheckoutMachine.CardConfirmed(this.items, this.card)

        CheckoutMachine.EmptyCart,
        is CheckoutMachine.CartWithItems,
        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardConfirmed,
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.applyCoupon(couponCode: String): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.CardConfirmed ->
            CheckoutMachine.CouponApplied(this.items, this.card, couponCode)
        CheckoutMachine.EmptyCart,
        is CheckoutMachine.CartWithItems,
        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardSelected,
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.placeOrder(): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.CouponApplied,
        is CheckoutMachine.CardConfirmed ->
            CheckoutMachine.OrderPlaced("someMadeUpOrderId")

        CheckoutMachine.EmptyCart,
        is CheckoutMachine.CartWithItems,
        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardSelected,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}

private fun CheckoutMachine.cancelCheckout(): CheckoutMachine {
    return when (this) {
        is CheckoutMachine.NoCard,
        is CheckoutMachine.CardSelected,
        is CheckoutMachine.CardConfirmed,
        is CheckoutMachine.CouponApplied ->
            CheckoutMachine.CartWithItems((this as HasItems).items)

        CheckoutMachine.EmptyCart,
        is CheckoutMachine.CartWithItems,
        is CheckoutMachine.OrderPlaced ->
            this
    }
}
