package everywhere.talk.checkout

data class CheckoutObject(
    val items: List<CartItem> = emptyList(),
    val card: Card? = null,
    val hasCheckoutBegun: Boolean = false,
    val isCardVerified: Boolean = false,
    val isComplete: Boolean = false
) {

    fun addItem(item: CartItem): CheckoutObject {
        if (hasCheckoutBegun) return this
        return this.copy(items = this.items + item)
    }

    fun selectCard(card: Card): CheckoutObject {
        if (!hasCheckoutBegun || isCardVerified || this.card != null) return this
        return this.copy(card = card)
    }

    fun verifyCard(): CheckoutObject {
        if (card == null || isComplete || isCardVerified) return this
        // ya know, actually do stuff here
        return this.copy(isCardVerified = true)
    }

    fun placeOrder(): CheckoutObject {
        if (!isCardVerified) return this
        // ya know, actually do stuff here
        return this.copy(isComplete = true)
    }

    fun cancel(): CheckoutObject {
        if (!hasCheckoutBegun || isComplete) return this
        return this.copy(hasCheckoutBegun = false, isCardVerified = false)
    }
}
