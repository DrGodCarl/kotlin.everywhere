package everywhere.talk.checkout

interface HasCard {
    val card: String
}

interface CheckoutEngine {
    fun checkout(cardHolder: HasCard): String
}

object DummyCheckoutEngine: CheckoutEngine {
    override fun checkout(cardHolder: HasCard) = "card: ${cardHolder.card}"
}

data class CartItem(
    val name: String
)

data class Card(
    val cardNumber: String
)
