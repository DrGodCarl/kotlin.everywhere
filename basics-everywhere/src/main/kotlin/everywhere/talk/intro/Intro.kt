package everywhere.talk.intro

data class MyDataToWrite(
    val id: Int,
    val importantData: String
)

val someData = MyDataToWrite(
    id = 1618,
    importantData = "I'm curious if anyone will ever read this."
)

enum class DataWriteMode {
    FILE_SYSTEM, S3, GOOGLE_STORAGE
}

data class DataWriteResult(
    val success: Boolean
)

/**
 * This method ONLY takes an argument of MyDataToWrite.
 * Also it CANNOT be null.
 * output indicates success
 */
fun writeSomeData(myData: Any?): Any? {
    val data = myData as? MyDataToWrite
    return data?.let {
        doWriteData(it.importantData)
    } ?: throw RuntimeException("Please read the documentation!")
}

// output indicates success
fun writeSomeData(myData: MyDataToWrite): Boolean =
    doWriteData(myData.importantData)


/**
 * The mode can be "FILE_SYSTEM", "S3", or "GOOGLE_STORAGE".
 * Case sensitive. Please be careful.
 * output indicates success
 */
fun writeSomeData(myData: MyDataToWrite, mode: String): Boolean {
    val verifiedMode = mode.takeIf { it in listOf("FILE_SYSTEM", "S3", "GOOGLE_STORAGE") }
        ?: throw RuntimeException("Why do I even bother writing documentation?!")
    return doWriteData(myData.importantData, verifiedMode)
}

// output indicates success
fun writeSomeData(myData: MyDataToWrite, mode: DataWriteMode): Boolean {
    return doWriteData(myData.importantData, mode)
}

fun theBestWriteSomeData(myData: MyDataToWrite, mode: DataWriteMode): DataWriteResult {
    return DataWriteResult(doWriteData(myData.importantData, mode))
}

private fun `function wherein I consume the above functions`() {
    val success = writeSomeData(someData, DataWriteMode.FILE_SYSTEM)
    if (success) {
        // This is okay but the only way to know to name your variable "success"
        // is by reading the docs.
    }

    val result = theBestWriteSomeData(someData, DataWriteMode.FILE_SYSTEM)
    if (result.success) {
        // This is better. Not only do you now know what the boolean represents,
        // the type can gain new fields over time and your code keeps working.
    }
}


private fun doWriteData(data: String, mode: String = ""): Boolean {
    return true
}

private fun doWriteData(data: String, mode: DataWriteMode): Boolean {
    return true
}
