package everywhere.talk.tickets

interface TicketState

interface MovableToToDo : TicketState
interface MovableToInProgress : TicketState
interface MoveableToCodeReview : TicketState
interface MovableToTesting : TicketState
interface MovableToDone : TicketState

// Phantom Types
object ToDo :
    MovableToInProgress

object InProgress :
    MovableToToDo,
    MoveableToCodeReview

object CodeReview :
    MovableToTesting

object Testing :
    MovableToInProgress,
    MovableToDone

object Done :
    TicketState

interface Ticket<T : TicketState> {
    val name: String
}

private data class TicketImpl<T : TicketState>(
    override val name: String
) : Ticket<T>

fun createTicket(name: String): Ticket<ToDo> {
    return TicketImpl(name)
}

// A fun hack around type erasure. Not really recommended for real code.
fun <T : TicketState, U : TicketState> Ticket<T>.transition(): Ticket<U> =
    this as Ticket<U>

inline fun <reified T : MovableToToDo> Ticket<T>.moveToToDo(): Ticket<ToDo> {
    return this.transition()
}

inline fun <reified T : MovableToInProgress> Ticket<T>.moveToInProgress(): Ticket<InProgress> {
    return this.transition()
}

inline fun <reified T : MoveableToCodeReview> Ticket<T>.moveToCodeReview(): Ticket<CodeReview> {
    return this.transition()
}

inline fun <reified T : MovableToTesting> Ticket<T>.moveToTesting(): Ticket<Testing> {
    return this.transition()
}

inline fun <reified T : MovableToDone> Ticket<T>.complete(): Ticket<Done> {
    return this.transition()
}

fun `function wherein I consume the above functions`() {
    val finalTicket = createTicket("Build a kanban board")
        .moveToInProgress()
        .moveToCodeReview()
        .moveToTesting()
        .moveToInProgress()
        .moveToCodeReview()
        .moveToTesting()
        .complete()
}
