package everywhere.talk.tickets

private enum class NormalTicketState {
    TO_DO, IN_PROGRESS, TESTING, DONE;

    val allowedTransitions: List<NormalTicketState>
        get() = when (this) {
            TO_DO -> listOf(IN_PROGRESS)
            IN_PROGRESS -> listOf(TO_DO, TESTING)
            TESTING -> listOf(IN_PROGRESS, DONE)
            DONE -> emptyList()
        }
}

private data class NormalTicket(
    val name: String,
    val state: NormalTicketState
)

private fun NormalTicket.moveToInProgress(): NormalTicket {
    if (this.state !in listOf(NormalTicketState.TO_DO, NormalTicketState.TESTING)) {
        throw RuntimeException("ILLEGAL STATE TRANSITION")
    }
    return this.copy(state = NormalTicketState.IN_PROGRESS)
}

private fun NormalTicket.transition(newState: NormalTicketState): NormalTicket {
    if (newState !in this.state.allowedTransitions) {
        throw RuntimeException("ILLEGAL STATE TRANSITION")
    }
    return this.copy(state = newState)
}
