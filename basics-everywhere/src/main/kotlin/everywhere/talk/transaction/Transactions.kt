package everywhere.talk.transaction

import java.lang.RuntimeException
import javax.validation.constraints.Size

data class TransactionTag(
    val relationType: String,
    val relationId: Long,
    val note: String? = null
)

data class LessGoodTransaction(
    val userId: Long,
    val amount: Int,
    // There is always at least one
    val tags: List<TransactionTag>
)

data class BetterTransaction(
    val userId: Long,
    val amount: Int,
    @field:Size(min = 1)
    val primaryTag: TransactionTag,
    val secondaryTags: List<TransactionTag> = emptyList()
)

fun saveTransaction(transaction: LessGoodTransaction) {
    if (transaction.tags.isEmpty()) throw RuntimeException("I was pretty clear in the docs...")
}

fun saveTransaction(transaction: BetterTransaction) {

}

private fun `function wherein I consume the above functions`() {
    val lessGoodTransaction = LessGoodTransaction(
        userId = 1618,
        amount = 314,
        tags = emptyList()
    )
    saveTransaction(lessGoodTransaction)

    val betterTransaction = BetterTransaction(
        userId = 1618,
        amount = 314,
        primaryTag = TransactionTag(
            relationType = "PURCHASE",
            relationId = 271828
        )
    )
    saveTransaction(betterTransaction)
}
